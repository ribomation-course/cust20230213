#include <iostream>
#include <thread>
#include <chrono>

using std::cout;
using namespace std::chrono_literals;

void usecase_thread() {
    cout << "[usecase_thread] start\n";
    auto thr = std::thread{[]() {
        cout << "[thread] Hello from a std::thread\n";
        std::this_thread::sleep_for(100ms);
        cout << "[thread] Hello again, from a std::thread\n";
    }};
    cout << "[usecase_thread] end\n";
}

void usecase_jthread() {
    cout << "[usecase_jthread] start\n";
    auto thr = std::jthread{[]() {
        cout << "[jthread] Hello from a std::jthread\n";
        std::this_thread::sleep_for(100ms);
        cout << "[jthread] Hello again, from a std::jthread\n";
    }};
    cout << "[usecase_jthread] end\n";
}


int main() {
    cout << "[main] start\n";
//    usecase_thread();
    usecase_jthread();
    cout << "[main] end\n";
}
