#include <iostream>
#include "range/v3/core.hpp"
#include "range/v3/view/filter.hpp"
#include "range/v3/view/transform.hpp"
#include "range/v3/view/iota.hpp"
using namespace std;
using namespace ranges::view;

int main() {
    auto      results = ints(1, 100)
                        | filter([](int n) { return n % 2 == 1; })
                        | transform([](int n) { return n * n; });
    for (auto n : results) cout << n << "  ";
    cout << endl;
    return 0;
}


