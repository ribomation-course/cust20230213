#include <iostream>
#include <string_view>
using namespace std::string_view_literals;
using std::cout;
using std::string_view;

int main() {
    char payload[] = "POST /api/customer/42 HTTP/1.1\r\nsome other stuff...";
    auto PAYLOAD   = string_view{payload};
    auto SPACE     = " "sv;
    auto CRNL      = "\r\n"sv;

    string_view operation, uri, protocol;
    auto start = 0UL;
    {
        auto end  = PAYLOAD.find(SPACE, start);
        auto N    = end - start;
        operation = PAYLOAD.substr(start, N);
        start += N + 1;
    }
    {
        auto end = PAYLOAD.find(SPACE, start);
        auto N   = end - start;
        uri      = PAYLOAD.substr(start, N);
        start += N + 1;
    }
    {
        auto end = PAYLOAD.find(CRNL, start);
        auto N   = end - start;
        protocol = PAYLOAD.substr(start, N);
    }
    cout << "HTTP: [" << operation << "] [" << uri  << "] [" << protocol << "]\n";
}

