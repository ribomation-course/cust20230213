#include <iostream>
#include <string>
#include <variant>

using namespace std::string_literals;
using std::cout;
using std::string;

struct AddVisitor {
    void operator ()(int& val) { val += val * 2; }
    void operator ()(double& val) { val += val * 1.25; }
    void operator ()(string& val) { val += val + "*|*"s; }
};

using V = std::variant<int, double, string>;

int main() {
    auto v = V{42};
    cout << std::boolalpha;
    cout << "check: int=" << std::holds_alternative<int>(v)
         << ", double=" << std::holds_alternative<double>(v) << "\n";

    v = 3.1415926;
    if (auto const* vptr = std::get_if<double>(&v); vptr != nullptr) {
        cout << "v contains a double\n";
    } else { cout << "v contains NOT a double\n"; }

    v = "I'm a text string"s;
    cout << "v.value=" << std::get<string>(v) << "\n";
    try {
        std::get<int>(v);
    } catch (std::bad_variant_access& x) {
        cout << "[ERR] " << x.what()
             << ", index=" << v.index()
             << "\n";
    }

    v = 10;
    std::visit(AddVisitor{}, v);
    cout << "int   : " << std::get<int>(v) << "\n";
    v = 2.5;
    std::visit(AddVisitor{}, v);
    cout << "double: " << std::get<double>(v) << "\n";
    v = "TEXT"s;
    std::visit(AddVisitor{}, v);
    cout << "string: " << std::get<string>(v) << "\n";
}

