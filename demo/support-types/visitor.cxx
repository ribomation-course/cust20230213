#include <iostream>
#include <string>
#include <variant>
#include <vector>
#include <numbers>

using namespace std::string_literals;
using std::cout;
using std::string;

template<typename... Types>
struct MATCH : Types ... {
    using Types::operator ()...;
};
template<typename... Ts> MATCH(Ts...) -> MATCH<Ts...>;

struct Rect   { int width{}, height{}; };
struct Square { int side{}; };
struct Circle { int radius{}; };
using Shape = std::variant<Rect, Square, Circle>;

int main() {
    auto shapes = std::vector<Shape>{
            Rect{.width=10, .height=5},
            Square{.side=5},
            Circle{.radius=5},
            Circle{.radius=25},
            Square{.side=15},
            Rect{.width=3, .height=2},
    };

    for (auto const& shape: shapes) {
        auto area = std::visit(MATCH{
                [](Rect   const& r) { return r.width * r.height; },
                [](Square const& s) { return s.side * s.side; },
                [](Circle const& c) { return static_cast<int>(std::numbers::pi * c.radius * c.radius); }
        }, shape);
        cout << "Area = " << area << "\n";
    }
}



