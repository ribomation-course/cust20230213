#include <iostream>
#include <sstream>
#include <string>
#include <utility>
#include <vector>
#include <algorithm>
#include <memory>
#include <cctype>

using std::cout;
using std::string;
using namespace std::string_literals;

class Account {
    string accno{};
    int balance{};
public:
    Account(string a, int b) : accno{std::move(a)}, balance{b} {}
    auto getBalance() const { return balance; }
    void update(int amount = 10) { balance *= amount; }
    friend auto operator <<(std::ostream& os, Account const& a) -> std::ostream& {
        return os << "Account{" << a.accno << ", " << a.balance << "}";
    }
};

auto to_upper(string s) {
    std::transform(s.begin(), s.end(), s.begin(), [](auto ch) {
        return ::toupper(ch);
    });
    return s;
}

auto create(int size) {
    auto accounts = std::vector<Account>{};
    accounts.reserve(size);
    for (int k = 0; k < size; ++k) {
        auto buf = std::ostringstream{};
        buf << "bank-" << (k * 1234) % 10000;
        accounts.emplace_back(to_upper(buf.str()), k * 25);
    }
    return accounts;
}

void handle(Account a) { cout << a << "\n"; }

int main(int argc, char** argv) {
    int N = argc == 1 ? 5 : std::stoi(argv[1]);
    auto accounts = create(N);
    for (auto&& a: accounts) cout << a << "\n";

    cout << "----\n";
    for (auto&& a: accounts) a.update();
    for (auto&& a: accounts) cout << a << "\n";

    cout << "----\n";
    auto a = std::make_unique<Account>("QWERTY"s, 100);
    handle(*a);
    a->update(-1);
    cout << *a << "\n";
    if (a->getBalance() < 0) return 0;
}

