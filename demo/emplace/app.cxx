#include <iostream>
#include <string>
#include <vector>
#include <utility>

using namespace std;
using namespace std::literals;


class Person {
    string   name;
    unsigned age;
public:
    Person(string name, unsigned age) noexcept : name{move(name)}, age{age} {
        cout << "Person{" << this->name << ", " << this->age << "} @ " << this << endl;
    }
    Person(Person&& that) noexcept : name{move(that.name)}, age{that.age} {
        that.age = 0;
        cout << "Person{&& " << name << ", " << age << "} @ " << this << endl;
    }
    ~Person() {
        cout << "~Person(" << name << ", " << age << ") @ " << this << endl;
    }
};


int main() {
    vector<Person> persons;
    persons.reserve(2);
    {
        cout << "--- Using push_back()\n";
        persons.push_back(Person{"Anna Conda"s, 42});
    }
    {
        cout << "\n--- Using emplace_back()\n";
        persons.emplace_back("Justin Time"s, 37);
    }
    cout << "\n--- main() end\n";
    return 0;
}

