#include <cstdio>
#include <string>
#include <chrono>
#include <random>
#include <vector>
#include <algorithm>
#include <execution>

using namespace std::string_literals;
using std::chrono::high_resolution_clock;
using std::chrono::duration_cast;
using TimeUnit = std::chrono::duration<double, std::milli>;

auto benchmark(std::string const& name, unsigned N, std::function<void()> const& stmts) {
    auto start_time = high_resolution_clock::now();
    stmts();
    auto end_time = high_resolution_clock::now();
    auto elapsed = duration_cast<TimeUnit>(end_time - start_time).count();
    ::printf("%s: %'d, elapsed %g ms\n", name.c_str(), N, elapsed);
    return elapsed;
}

int main(int argc, char** argv) {
    auto N = (argc == 1) ? 10'000U : std::stoi(argv[1]);
    auto data = std::vector<int>{};
    data.reserve(N);
    auto R = std::random_device{};
    auto r = std::mt19937_64{R()};
    auto rand = std::normal_distribution{1000., 500.};
    for (auto k = 0U; k < N; ++k) data.push_back(static_cast<int>(rand(r)));

    auto seq_ms = benchmark("sequential"s, N, [data]() mutable {
        std::sort(/*std::execution::seq, */data.begin(), data.end());
    });
    auto par_ms = benchmark("parallel  "s, N, [data]() mutable {
        std::sort(std::execution::par, data.begin(), data.end());
    });
    ::printf("parallel was %s\n", (par_ms < seq_ms) ? "faster" : "slower");
}

