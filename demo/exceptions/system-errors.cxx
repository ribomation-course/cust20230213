#include <iostream>
#include <iomanip>
#include <string>
#include <system_error>
#include <cmath>
#include <cerrno>
#include <cstring>
using namespace std;

int main() {
    auto result = log10(0);

    cout << boolalpha;
    cout << "result  : " << result << endl;
    cout << "valid   : " << isfinite(result) << endl;
    cout << "infinity: " << isinf(result) << endl;

    cout << "C   err : " << strerror(errno) << " (" << errno << ")" << endl;

    error_condition err = system_category().default_error_condition(errno);
    cout << "C++ err : " << err.message() << " (" << err.value() << ")" << endl;

    return 0;
}


