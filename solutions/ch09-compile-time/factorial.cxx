#include <iostream>

consteval auto factorial(unsigned n) {
    if (n == 0U) return 0UL;
    if (n == 1U) return 1UL;
    return n * factorial(n - 1);
}

int main() {
    unsigned long tbl[] = {
            factorial(1),
            factorial(2),
            factorial(3),
            factorial(4),
            factorial(5)
    };
    using std::cout;
    for (auto k : tbl) cout << k << " ";
    cout << "\n";
}
