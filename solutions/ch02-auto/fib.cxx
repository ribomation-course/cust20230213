#include <iostream>
#include <string>
#include <tuple>
#include <map>

using std::cout;

auto fib(int n) {
    if (n == 0) return 0UL;
    if (n == 1) return 1UL;
    return fib(n - 2) + fib(n - 1);
}

auto compute(int n) -> std::tuple<int, unsigned long>;

auto table(int n) {
    auto tbl = std::map<int, unsigned long>{};
    for (auto k=1; k<=n; ++k) {
        auto [a, f] = compute(k);
        tbl[k] = f;
    }
    return tbl;
}

int main(int argc, char** argv) {
    auto N = argc == 1 ? 10 : std::stoi(argv[1]);
    {
        auto f = fib(N);
        cout << "fib(" << N << ") = " << f << "\n";
    }
    {
        auto [a, f] = compute(N);
        cout << "fib(" << a << ") = " << f << "\n";
    }
    cout << "----\n";
    {
        for (auto [a, f]: table(N))
            cout << "fib(" << a << ") = " << f << "\n";
    }
}

auto compute(int n) -> std::tuple<int, unsigned long> {
    return std::make_tuple(n, fib(n));
}
