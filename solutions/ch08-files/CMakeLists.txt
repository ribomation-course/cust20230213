cmake_minimum_required(VERSION 3.22)
project(ch08_files)

set(CMAKE_CXX_STANDARD 20)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(line-count line-count.cxx)
target_compile_options(line-count PRIVATE ${WARN})


