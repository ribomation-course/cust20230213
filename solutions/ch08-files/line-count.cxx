#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <filesystem>
#include <string>
#include <unordered_set>
#include <stdexcept>

namespace fs = std::filesystem;
using namespace std::string_literals;
using std::cout;
using std::string;

struct Count {
    string const name;
    unsigned lines = 0;
    unsigned words = 0;
    unsigned chars = 0;

    explicit Count(string name) : name{std::move(name)} {}
    explicit Count(fs::path const& file) : name{file.string()} { update(file); }

    void update(fs::path const& file) {
        auto f = std::ifstream{file};
        chars += fs::file_size(file);
        for (string line; getline(f, line);) {
            ++lines;
            auto buf = std::istringstream{line};
            for (string word; buf >> word;) ++words;
        }
    }

    void update(const Count& cnt) {
        chars += cnt.chars;
        words += cnt.words;
        lines += cnt.lines;
    }

    void operator +=(fs::path const& file) { update(file); }
    void operator +=(const Count& cnt) { update(cnt); }
};

auto operator <<(std::ostream& os, Count const& cnt) -> std::ostream& {
    using std::setw;
    auto const MAX = 50UL;
    auto const NAME = cnt.name.size();
    return os << setw(6) << cnt.lines
              << setw(8) << cnt.words
              << setw(10) << cnt.chars
              << "\t" << cnt.name.substr(NAME - std::min(MAX, NAME));
}

bool is_text(fs::path const& file) {
    if (file.string().starts_with("CMake")) return false;
    static auto const TXT = std::unordered_set<string>{".txt", ".cxx", ".hxx"};
    return TXT.contains(file.extension().string());
}

int main(int argc, char** argv) {
    auto dir = argc == 1 ? fs::current_path() : fs::path{argv[1]};
    if (not fs::is_directory(dir))
        throw std::invalid_argument{"not a directory "s + dir.string()};

    cout << "Dir: " << fs::canonical(dir) << "\n";
    auto total = Count{"TOTAL"s};
    for (auto const& e: fs::recursive_directory_iterator{dir}) {
        auto const& file = e.path();
        if (fs::is_regular_file(file) && is_text(file.filename())) {
            auto cnt = Count{file};
            cout << cnt << "\n";
            total += cnt;
        }
    }
    cout << total << "\n";
}
