#include <iostream>
#include <fstream>
#include <string>
#include <functional>
#include <chrono>
#include <unordered_set>
#include <set>

namespace cr = std::chrono;
using namespace std::string_literals;
using std::cout;
using std::string;

void elapsed(const std::function<void()>& stmts);

int main(int argc, char** argv) {
    auto filename = (argc == 1) ? "../musketeers.txt"s : argv[1];
    elapsed([&filename]{
        auto file = std::ifstream{filename};
        if (!file) throw std::invalid_argument{"cannot open "s + filename};

        auto      words = std::unordered_multiset<string>{};
        for (string word{}; file >> word;) {
            if (word.size() >= 4) words.insert(word);
        }

        auto freqs = std::multiset<string>{words.begin(), words.end()};
        for (auto it = freqs.begin(); it != freqs.end();) {
            auto word  = *it;
            auto count = freqs.count(word);
            cout << word << ": " << count << "\n";
            advance(it, count);
        }
    });
}

void elapsed(const std::function<void()>& stmts) {
    auto startTime = cr::high_resolution_clock::now();
    stmts();
    auto endTime = cr::high_resolution_clock::now();
    auto elapsed = cr::duration_cast<cr::milliseconds>(endTime - startTime);
    cout << "Elapsed Time: " << elapsed.count() << " ms\n";
}
