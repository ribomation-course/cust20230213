#include <iostream>

using std::cout;

template</*typename T,*/ typename... Args>
auto multiply(/*T t,*/ Args... args) {
    return (/*t **/ ... * args);
}

int main() {
    cout << "(1) " << multiply(42) << "\n";
    cout << "(2) " << multiply(2, 21) << "\n";
    cout << "(3) " << multiply(2, 3, 7) << "\n";
    cout << "(4) " << multiply(2, 3, 7, 1) << "\n";
    cout << "(5) " << multiply(1.123, 3, 7, 2, 1) << "\n";
    cout << "(6) " << multiply(1, 3, 7, 2, 1.123) << "\n";
}
