cmake_minimum_required(VERSION 3.22)
project(ch05_variadics)

set(CMAKE_CXX_STANDARD 20)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(variadics mult.cxx)
target_compile_options(variadics PRIVATE ${WARN})


