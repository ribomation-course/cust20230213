#include <iostream>
#include <fstream>
#include <string>
#include <set>
#include <memory_resource>
#include <cstdint>

using std::cout;
using std::pmr::string;
using std::pmr::set;

auto operator new(size_t nbytes) -> void* {
    std::cerr << "oops, trying to sneak in system heap space of size " << nbytes << "\n";
    throw std::bad_alloc{};
}

auto operator new[](size_t nbytes) -> void* {
    std::cerr << "oops, trying to sneak in system heap array space of size " << nbytes << "\n";
    throw std::bad_alloc{};
}

int main() {
    std::uint8_t storage[100'000]{};
    auto upstream = std::pmr::null_memory_resource();
    auto buffer = std::pmr::monotonic_buffer_resource{std::data(storage), std::size(storage), upstream};
    auto memory = std::pmr::unsynchronized_pool_resource{&buffer};
    std::pmr::set_default_resource(&memory);

    cout << "[main] start\n";
    {
        //auto file = std::ifstream{"../pmr-words.cxx"};
        auto words = set<string>{};
        for (string word; std::cin >> word;) {
            words.insert(std::move(word));
        }
        for (auto const& w: words) std::cout << w << "\n";
    }
    cout << "[main] done\n";

    try {
        new int{666};
    } catch (std::bad_alloc& x) {
        printf("*** Confirmed!\nNo system-heap space was harmed in this production.\n%s\n", x.what());
    }
}
