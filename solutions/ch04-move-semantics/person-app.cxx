#include <iostream>
#include "person.hxx"

namespace rm = ribomation::domain;
using std::cout;

void pass_by_reference(rm::Person const& q) {
    cout << "[by-ref] person: " << q << "\n";
}

void dummy(rm::Person q) {
    cout << "[dummy] person: " << q << "\n";
}

void pass_by_value(rm::Person q) {
    cout << "[by-val] person: " << q << "\n";
    dummy(std::move(q));
}

int main() {
    auto pelle = rm::Person{"Pelle"};
    cout << "[main] (1) person: " << pelle << "\n";

    pass_by_reference(pelle);
    cout << "[main] (2) person: " << pelle << "\n";

    pass_by_value(std::move(pelle));
    cout << "[main] (3) person: " << pelle << "\n";
}
