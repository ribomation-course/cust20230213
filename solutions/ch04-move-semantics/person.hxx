#pragma once

#include <iostream>
#include <cstdlib>
#include <cstring>

namespace ribomation::domain {

   inline auto addr(char* s) {
        return reinterpret_cast<unsigned long>(s);
    }

    class Person {
        char* name = nullptr;
    public:
        explicit Person(char const* name_) : name{::strdup(name_)} {
            std::cout << "Person(char*) &name=" << addr(name) << " @ " << this << "\n";
        }
        ~Person() {
            std::cout << "~Person() &name=" << addr(name) << " @ " << this << "\n";
            ::free(name);
        }

        Person(Person const&) = delete;
        auto operator =(Person const&) -> Person& = delete;

        Person(Person&& rhs) noexcept: name{rhs.name} {
            rhs.name = nullptr;
            std::cout << "Person(&&) &name=" << addr(name) << " @ " << this << "\n";
        }
        auto operator =(Person&& rhs) noexcept -> Person& {
            if (this != &rhs) {
                ::free(name);
                name = rhs.name;
                rhs.name = nullptr;
            }
            return *this;
        }

        friend auto operator <<(std::ostream& os, Person const& p) -> std::ostream& {
            return os << "Person{" << (p.name == nullptr ? "?" : p.name) << "}";
        }
    };

}

