#include <iostream>
#include <functional>

using std::cout;

auto count_if(int* first, int* last, std::function<bool(int)> pred) {
    auto cnt = 0;
    for (; first != last; ++first) if (pred(*first)) ++cnt;
    return cnt;
}

int main() {
    {
        int numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        auto const N = sizeof(numbers) / sizeof(numbers[0]);

        auto pr = [](int n) { cout << n << " "; };
        std::for_each(numbers, numbers + N, pr);
        cout << "\n";
        //auto factor = 42;
        std::transform(numbers, numbers + N, numbers, [factor = 42](int n) {
            return factor * n;
        });
        std::for_each(numbers, numbers + N, pr);
        cout << "\n";
    }

    {
        int numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        auto const N = sizeof(numbers) / sizeof(numbers[0]);
        auto cnt = count_if(numbers, numbers + N, [](int n) {
            return n % 2 == 0;
        });
        cout << "cnt = " << cnt << "\n";
    }
}
