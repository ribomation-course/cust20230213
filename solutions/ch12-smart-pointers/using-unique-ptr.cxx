#include <iostream>
#include "using-unique-ptr.hxx"
using namespace std::string_literals;
using std::cout;
using std::endl;

void use_unique_ptr() {
    auto anna = std::make_unique<rm::Person>("Anna Conda"s, 42);
    cout << "[use_unique_ptr] anna: " << *anna << endl;

    auto p = func1(std::move(anna));

    cout << "[use_unique_ptr] p: " << *p << endl;
    cout << "[use_unique_ptr] anna: " << anna.get() << endl;
}

auto func1(std::unique_ptr<rm::Person> q) -> std::unique_ptr<rm::Person> {
    q->incrAge();
    cout << "[func1] q: " << *q << endl;
    return func2(std::move(q));
}

auto func2(std::unique_ptr<rm::Person> q) -> std::unique_ptr<rm::Person> {
    q->incrAge();
    cout << "[func2] q: " << *q << endl;
    return q;
}


