#include "using-unique-ptr.hxx"
#include "using-shared-ptr.hxx"

int main() {
    use_unique_ptr();
    std::cout << "----\n";
    use_shared_ptr();
}
