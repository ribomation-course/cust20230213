#pragma once
#include <memory>
#include "person.hxx"
namespace rm = ribomation::domain;

void use_shared_ptr();
auto func2(std::shared_ptr<rm::Person> q) -> std::shared_ptr<rm::Person>;
auto func1(std::shared_ptr<rm::Person> q) -> std::shared_ptr<rm::Person>;

