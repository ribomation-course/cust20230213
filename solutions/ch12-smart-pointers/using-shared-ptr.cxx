#include <iostream>
#include "using-shared-ptr.hxx"
using namespace std::string_literals;
using std::cout;
using std::endl;

void use_shared_ptr() {
    auto justin = std::make_shared<rm::Person>("Justin Time"s, 37);
    cout << "[use_shared_ptr] justin: " << *justin << " [" << justin.use_count() << "]" << endl;
    {
        auto p = func1(justin);
        cout << "[use_shared_ptr] p: " << *p << " [" << p.use_count() << "]" << endl;
    }
    cout << "[use_shared_ptr] justin: " << *justin << " [" << justin.use_count() << "]" << endl;
}

auto func1(std::shared_ptr<rm::Person> q) -> std::shared_ptr<rm::Person> {
    q->incrAge();
    cout << "[func1] q: " << *q << " [" << q.use_count() << "]" << endl;
    return func2(q);
}

auto func2(std::shared_ptr<rm::Person> q) -> std::shared_ptr<rm::Person> {
    q->incrAge();
    cout << "[func2] q: " << *q << " [" << q.use_count() << "]" << endl;
    return q;
}
