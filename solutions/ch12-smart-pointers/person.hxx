#pragma once

#include <iostream>
#include <string>
#include <utility>

namespace ribomation::domain {
    using std::string;
    using std::cout;

    class Person {
        const string name;
        unsigned     age;
    public:
        Person(string name_, unsigned int age_) : name{std::move(name_)}, age{age_} {
            cout << "+Person{" << name << ", " << age << "} @ " << this << "\n";
        }

        ~Person() {
            cout << "~Person() @ " << this << "\n";
        }

        Person(const Person&) = delete;
        auto operator=(const Person&) -> Person& = delete;

        void incrAge() { ++age; }

        friend auto operator <<(std::ostream& os, const Person& p) -> std::ostream& {
            return os << "Person{" << p.name << ", " << p.age << "} @ " << &p;
        }
    };

}

