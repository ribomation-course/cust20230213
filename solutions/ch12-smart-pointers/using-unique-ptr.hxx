#pragma once
#include <memory>
#include "person.hxx"
namespace rm = ribomation::domain;

void use_unique_ptr();

auto func2(std::unique_ptr <rm::Person> q) -> std::unique_ptr<rm::Person>;

auto func1(std::unique_ptr <rm::Person> q) -> std::unique_ptr<rm::Person>;

