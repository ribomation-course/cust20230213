#include <iostream>
#include <string>
#include <vector>
using std::cout;
using std::string;
using namespace std::string_literals;

int main() {
    {
        std::vector<string> words = {
                "C++"s, "is"s, "a"s, "cool"s, "programming"s, "language"s, "!"s
        };
        for (string const& w: words) cout << w << " ";
        cout << "\n";
    }
    {
        string sentence = "It was a dark and stormy night. Suddenly, there was a sound of a shot."s;
        string phrase = "sound"s;
        if (auto idx = sentence.find(phrase); idx != string::npos)
            cout << "found: " << sentence.substr(idx) << "\n";
        else cout << phrase << " not found\n";
        phrase = "hound"s;
        if (auto idx = sentence.find(phrase); idx != string::npos)
            cout << "found: " << sentence.substr(idx) << "\n";
        else cout << phrase << " not found\n";
    }
}
