#include <iostream>
#include <thread>
#include <syncstream>
#include "message-queue.hxx"
namespace rm = ribomation::concurrent;
using std::cout;

int main() {
    auto const N = 100U;
    auto q = rm::MessageQueue<unsigned>{8};

    auto receiver = [&q]() {
        for (auto msg = q.get(); msg != 0; msg = q.get()) {
            std::osyncstream{cout} << "        [recv] " << msg << "\n";
        }
        std::osyncstream{cout} << "[recv] done\n";
    };

    auto sender = [&q](){
        std::osyncstream{cout} << "[send] started\n";
        for (auto msg = 1U; msg <= N; ++msg) {
            std::osyncstream{cout} << "[send] " << msg << "\n";
            q.put(msg);
        }
        q.put(0);
        std::osyncstream{cout} << "[send] done\n";
    };

    std::osyncstream{cout} << "[main] start\n";
    {
        auto r = std::jthread{receiver};
        auto s = std::jthread{sender};
    }
    std::osyncstream{cout} << "[main] end\n";
}
