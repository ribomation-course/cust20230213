#include <iostream>
#include <thread>
#include <syncstream>
#include "message-queue.hxx"

namespace rm = ribomation::concurrent;
using std::cout;

int main() {
    auto const N = 100U;
    auto q1 = rm::MessageQueue<unsigned>{8};
    auto q2 = rm::MessageQueue<unsigned>{8};

    auto receiver = [&q2]() {
        for (auto msg = q2.get(); msg != 0; msg = q2.get()) {
            std::osyncstream{cout} << "                [recv] " << msg << "\n";
        }
        std::osyncstream{cout} << "[recv] done\n";
    };

    auto transformer = [&q1, &q2]() {
        for (auto msg = q1.get(); msg != 0; msg = q1.get()) {
            msg <<= 1;
            std::osyncstream{cout} << "        [trans] " << msg << "\n";
            q2.put(msg);
        }
        q2.put(0);
        std::osyncstream{cout} << "[trans] done\n";
    };

    auto sender = [&q1]() {
        std::osyncstream{cout} << "[send] started\n";
        for (auto msg = 1U; msg <= N; ++msg) {
            std::osyncstream{cout} << "[send] " << msg << "\n";
            q1.put(msg);
        }
        q1.put(0);
        std::osyncstream{cout} << "[send] done\n";
    };

    std::osyncstream{cout} << "[main] start\n";
    {
        auto prod = std::jthread{receiver};
        auto trans = std::jthread{transformer};
        auto cons = std::jthread{sender};
    }
    std::osyncstream{cout} << "[main] end\n";
}
