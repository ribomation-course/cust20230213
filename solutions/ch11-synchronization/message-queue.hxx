#pragma once
#include <mutex>
#include <condition_variable>
#include <queue>

namespace ribomation::concurrent {
    using std::mutex;
    using std::condition_variable;
    using std::queue;
    using std::unique_lock;

    template<typename T>
    class MessageQueue {
        mutex              lck;
        condition_variable notEmpty;
        condition_variable notFull;
        queue<T>           inbox;
        const unsigned     capacity;

        [[nodiscard]] bool empty() const { return inbox.size() == 0; }
        [[nodiscard]] bool full()  const { return inbox.size() == capacity; }

    public:
        explicit MessageQueue(unsigned max=16) : capacity{max} {}

        MessageQueue(const MessageQueue<T>&) = delete;
        MessageQueue<T>& operator =(const MessageQueue<T>&) = delete;

        void put(T x) {
            auto g = unique_lock<mutex>{lck};
            notFull.wait(g, [this] { return !full(); });
            inbox.push(x);
            notEmpty.notify_all();
        }

        T get() {
            auto g = unique_lock<mutex>{lck};
            notEmpty.wait(g, [this] { return !empty(); });
            T x = inbox.front();
            inbox.pop();
            notFull.notify_all();
            return x;
        }
    };
}

