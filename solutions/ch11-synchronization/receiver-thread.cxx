#include <iostream>
#include <thread>
#include <syncstream>
#include "message-queue.hxx"
namespace rm = ribomation::concurrent;
using std::cout;

int main() {
    auto const N = 100U;
    auto q = rm::MessageQueue<unsigned>{8};

    auto receiver = [&q]() {
        for (auto msg = q.get(); msg != 0; msg = q.get()) {
            std::osyncstream{cout} << "[recv] " << msg << "\n";
        }
    };

    std::osyncstream{cout} << "[main] start\n";
    {
        auto t = std::jthread{receiver};
        for (auto k = 1U; k <= N; ++k) q.put(k);
        q.put(0);
    }
    std::osyncstream{cout} << "[main] end\n";
}
