#include <iostream>
#include <thread>
#include <syncstream>
#include <vector>
#include <chrono>
#include <string>
using namespace std::string_literals;
using namespace std::chrono_literals;
using std::cout;
using std::string;

int main() {
    auto N = std::thread::hardware_concurrency();
    cout << "[main] # CPU = " << N << "\n";

    auto body = [](unsigned id, unsigned n) {
        auto const ID = id;
        string tab = ""s;
        while (--id > 0) tab += "      "s;
        do {
            std::osyncstream{cout} << tab
            << "[thr-" << ID << "] n=" << n << "\n";
            std::this_thread::sleep_for(.25s);
        } while (--n > 0);
    };

    cout << "[main] creating threads\n";
    {
        auto threads = std::vector<std::jthread>{};
        threads.reserve(N);
        for (auto k = 1U; k <= N; ++k) threads.emplace_back(body, k, 10);
    }
    cout << "[main] done\n";
}
