#include <iostream>
#include <numeric>
#include <algorithm>
#include <vector>
#include "helpers.hxx"

auto mk_letters() {
    auto letters = std::string(2 * 26, ' ');
    std::iota(letters.begin(), letters.begin() + 26, 'a');
    std::iota(letters.begin() + 26, letters.end(), 'A');
    return letters + "’";
}

auto aggregate(std::string_view payload, unsigned min_size) -> WordFreqs {
    auto const letters = mk_letters();
    auto const not_found = std::string_view::npos;
    auto freqs = WordFreqs{};
    auto start = 0UL;
    do {
        start = payload.find_first_of(letters, start);
        if (start == not_found) break;

        auto end = payload.find_first_not_of(letters, start);
        if (end == not_found) break;

        auto size = end - start;
        auto word = payload.substr(start, size);
        start = end;

        if (word.size() >= min_size) ++freqs[word];
    } while (start < payload.size());

    return freqs;
}

void print(WordFreqs const& freqs, unsigned max_words) {
    using wf = std::pair<std::string_view, unsigned>;
    auto sortable = std::vector<wf>{freqs.begin(), freqs.end()};

    auto comparator = [](wf const& lhs, wf const& rhs) {
        return lhs.second > rhs.second;
    };
    std::partial_sort(sortable.begin(), sortable.begin() + max_words, sortable.end(), comparator);

    std::for_each_n(sortable.begin(), max_words, [](wf const& p) {
        std::cout << p.first << ": " << p.second << "\n";
    });
}
