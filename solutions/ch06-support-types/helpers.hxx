#pragma once
#include <string>
#include <string_view>
#include <unordered_map>

using WordFreqs = std::unordered_map<std::string_view, unsigned>;

auto aggregate(std::string_view payload, unsigned min = 4) -> WordFreqs;
void print(WordFreqs const& freqs, unsigned max_words = 25);

