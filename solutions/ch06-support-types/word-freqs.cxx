#include <iostream>
#include <fstream>
#include <filesystem>
#include <chrono>
#include <stdexcept>
#include "helpers.hxx"

namespace cr = std::chrono;
namespace fs = std::filesystem;
using namespace std::string_literals;
using std::cout;

auto load(std::string const& filename) -> std::string_view;

int main(int argc, char** argv) {
    auto filename = argc == 1 ? "../musketeers.txt"s : std::string{argv[1]};
    cout << "filename: " << filename << "\n";
    if (not fs::exists(filename)) throw std::invalid_argument{"not found: "s + filename};

    auto startTime = cr::high_resolution_clock::now();
    {
        auto payload = load(filename);
        cout << "loaded: " << payload.size() << " chars\n";

        auto freqs = aggregate(payload);
        cout << "aggregated: " << freqs.size() << " words\n";

        cout << "----\n";
        print(freqs);
        delete[] payload.data();
    }
    cout << "----\n";
    auto endTime = cr::high_resolution_clock::now();
    auto elapsedTime = cr::duration_cast<cr::milliseconds>(endTime - startTime);
    cout << "elapsed " << elapsedTime.count() << " ms\n";

}

auto load(std::string const& filename) -> std::string_view {
    auto size = fs::file_size(fs::path{filename});

    auto f = std::ifstream{filename};
    if (!f) throw std::invalid_argument{"cannot open "s + filename};

    auto buf = new char[size];
    f.read(buf, static_cast<long>(size));
    return std::string_view{buf, size};
}


