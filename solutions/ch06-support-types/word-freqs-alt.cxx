#include "memory-mapped-file.hxx"
#include "helpers.hxx"

namespace cr = std::chrono;
namespace fs = std::filesystem;
namespace rm = ribomation::io;
using namespace std::string_literals;
using std::cout;

int main(int argc, char** argv) {
    auto filename = argc == 1 ? "../musketeers.txt"s : std::string{argv[1]};
    cout << "filename: " << filename << "\n";
    if (not fs::exists(filename)) throw std::invalid_argument{"not found: "s + filename};

    auto startTime = cr::high_resolution_clock::now();
    {
        auto f = rm::MemoryMappedFile{filename};
        cout << "loaded: " << f.size() << " chars\n";

        auto freqs = aggregate(f.data());
        cout << "aggregated: " << freqs.size() << " words\n";

        cout << "----\n";
        print(freqs);
    }
    cout << "----\n";
    auto endTime = cr::high_resolution_clock::now();
    auto elapsedTime = cr::duration_cast<cr::milliseconds>(endTime - startTime);
    cout << "elapsed " << elapsedTime.count() << " ms\n";
}
