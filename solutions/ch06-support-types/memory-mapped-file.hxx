#pragma once

#include <iostream>
#include <string>
#include <stdexcept>
#include <filesystem>

#include <cstring>
#include <utility>

#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

namespace ribomation::io {
    namespace fs = std::filesystem;
    using std::string;

    class MemoryMappedFile {
        const std::string filename;
        char* payload;
        size_t payloadSize;

        void load_into_memory() {
            int fd = ::open(filename.c_str(), O_RDONLY);
            if (fd < 0) throw std::invalid_argument{"cannot open '" + filename + "': " + strerror(errno)};

            payloadSize = fs::file_size(filename);
            payload = (char*) ::mmap(nullptr, payloadSize, PROT_READ, MAP_PRIVATE, fd, 0);
            if (payload == MAP_FAILED) throw std::runtime_error(string("failed to mmap: ") + strerror(errno));

            ::close(fd);
        }

    public:
        MemoryMappedFile(string  filename_) : filename{std::move(filename_)} {
            load_into_memory();
        }

        ~MemoryMappedFile() {
            ::munmap(payload, payloadSize);
        }

        MemoryMappedFile(const MemoryMappedFile &) = delete;
        MemoryMappedFile &operator=(const MemoryMappedFile &) = delete;

        auto size() const { return payloadSize; }
        auto data() const -> std::string_view {
            return std::string_view{payload, payloadSize};
        }
    };


}
